FROM ruby:latest

ENV BUNDLE_VERSION=2.1.4

# ENV USER_ID 1000
# ENV GROUP_ID 1000

# RUN addgroup --gid $GROUP_ID user
# RUN adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID user

# ENV INSTALL_PATH /opt/app
# ENV BUNDLE_PATH /bundle
ENV BUNDLE_JOBS 4
ENV BUNDLE_RETRY 4
# ENV DISABLE_SPRING 1

# RUN mkdir -p $INSTALL_PATH
RUN mkdir -p /app

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash \
&& curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
&& echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee \
/etc/apt/sources.list.d/yarn.list \
&& apt-get update \
&& apt-get install -y nodejs yarn \
libpq-dev postgresql postgresql-contrib postgresql-client \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN gem install rails bundler:$BUNDLE_VERSION
# RUN chown -R user:user /opt/app
# RUN chown -R user:user /app

# USER $USER_ID
CMD ["/bin/sh"]